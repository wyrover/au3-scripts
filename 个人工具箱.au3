#cs ----------------------------------------------------------------------------

 AutoIt 版本: 3.2.4.9(第一版)
 脚本作者: 
	Email: 
	QQ/TM: 
 脚本版本: 
 脚本功能: 

#ce ----------------------------------------------------------------------------

; 脚本开始 - 在这后面添加您的代码.
include <GUIConstants.au3>
AutoItSetOption("TrayIconHide", 1)
AutoItSetOption("RunErrorsFatal", 1)
$but12 = IniRead("Tools.ini", "Tab1", "but2", "")
$path12 = IniRead("Tools.ini", "Tab1", "path2", "")
$but13 = IniRead("Tools.ini", "Tab1", "but3", "")
$path13 = IniRead("Tools.ini", "Tab1", "path3", "")
$but14 = IniRead("Tools.ini", "Tab1", "but4", "")
$path14 = IniRead("Tools.ini", "Tab1", "path4", "")
$but15 = IniRead("Tools.ini", "Tab1", "but5", "")
$path15 = IniRead("Tools.ini", "Tab1", "path5", "")
$but16 = IniRead("Tools.ini", "Tab1", "but6", "")
$path16 = IniRead("Tools.ini", "Tab1", "path6", "")
$but17 = IniRead("Tools.ini", "Tab1", "but7", "")
$path17 = IniRead("Tools.ini", "Tab1", "path7", "")
$but18 = IniRead("Tools.ini", "Tab1", "but8", "")
$path18 = IniRead("Tools.ini", "Tab1", "path8", "")
GUICreate("我的常用工具集 By:AC",350,200,350,200)
GUISetBkColor(0x00E0FFFF)
GUISetFont(9, 300)
$tab=GUICtrlCreateTab (0,0, 350,200)
GUICtrlSetState(-1,$GUI_SHOW)
$tab1=GUICtrlCreateTabitem ("    常用工具    ")
$tab11=GUICtrlCreateButton ("CMD", 20,30,150,30)
$tab12=GUICtrlCreateButton ($but12, 180,30,150,30)
$tab13=GUICtrlCreateButton ($but13, 20,65,150,30)
$tab14=GUICtrlCreateButton ($but14, 180,65,150,30)
$tab15=GUICtrlCreateButton ($but15, 20,100,150,30)
$tab16=GUICtrlCreateButton ($but16, 180,100,150,30)
$tab17=GUICtrlCreateButton ($but17, 20,135,150,30)
$tab18=GUICtrlCreateButton ($but18, 180,135,150,30)
$tab2=GUICtrlCreateTabitem ("    远程连接    ")
$icon = GUICtrlCreateIcon ("shell32.dll",10, 180,30)
$icon = GUICtrlCreateIcon ("My-com.ico",30, 20,30)
GUICtrlCreateLabel ("连接远程桌面",60, 40, 80)
GUICtrlCreateLabel ("映射网络磁盘",220, 40, 80)
$tab21=GUICtrlCreateButton ("电影服务器", 20,70,150,30)
$tab22=GUICtrlCreateButton ("游戏服务器", 20,105,150,30)
$tab23=GUICtrlCreateButton ("无盘服务器", 20,140,150,30)
$tab24=GUICtrlCreateButton ("netrw", 180,70,150,30)
$tab25=GUICtrlCreateButton ("game$+game2$+menu$", 180,105,150,30)
$tab26=GUICtrlCreateButton ("lc", 180,140,150,30)
GUICtrlCreateTabitem ("")
$tab3=GUICtrlCreateTabitem ("    DDos测试    ")
$Tab31 = GUICtrlCreateButton ("DDos之Fping", 20, 40, 180, 30)
GUISetState ()  
While 1
   $msg1 = GUIGetMsg()
   Select
       Case $msg1 = $GUI_EVENT_CLOSE
           Exit
       Case $msg1 = $tab11
    $destination = @SystemDir & "\cmd.cmd"
          FileInstall("cmd.cmd", $destination)
    FileChangeDir(@SystemDir)
          Run(""&@SystemDir&"\cmd.cmd")            
       Case $msg1 = $tab12
           Run(""&$path12&"")
       Case $msg1 = $tab13
           Run(""&$path13&"")
       Case $msg1 = $tab14
           Run(""&$path14&"")
       Case $msg1 = $tab15
          Run(""&$path15&"")
       Case $msg1 = $tab16
          Run(""&$path16&"")
       Case $msg1 = $tab17
          Run(""&$path17&"")
       Case $msg1 = $tab18
          Run(""&$path18&"")
       Case $msg1 = $tab21           
          Runwait(""&@SystemDir&"\mstsc.exe /v 192.168.0.225")
       Case $msg1 = $tab22
          $destination = @TempDir & "\putty.exe"
          FileInstall("putty.exe", $destination)
          Run(""&$destination&" 192.168.0.252:23")
       Case $msg1 = $tab23
          $destination = @TempDir & "\NZWER.EXE"
          FileInstall("NZWER.EXE", $destination)
          Run(""&$destination&" 192.168.0.251:1")
       Case $msg1 = $tab24           
          $passwd = InputBox("权限核查", "请输入密码：", "", "*",50,50)
    If @Error = 0 Then Run("net use Z: \\mm\netrw "&$passwd&" /user:110","",@SW_Hide)
       Case $msg1 = $tab25
          $passwd = InputBox("权限核查", "请输入密码：", "", "*",50,50)
          If @Error = 0 Then 
              Run("net use Y: \\ecofe2\game$ "&$passwd&" /user:update","",@SW_Hide)
              Run("net use X: \\ecofe2\game2$ "&$passwd&" /user:update","",@SW_Hide)
              Run("net use W: \\ecofe2\menu$ "&$passwd&" /user:update","",@SW_Hide)
    Endif
       Case $msg1 = $tab26
          $passwd = InputBox("权限核查", "请输入密码：", "", "*",50,50)
          If @Error = 0 Then Run("net use V: \\mm\lc "&$passwd&" /user:110","",@SW_Hide)
       Case $msg1 = $tab31
          $destination3 = @TempDir & "\Fping.exe"
          FileInstall("Fping.exe", $destination3)
          Run(""&$destination3&"")
   EndSelect
Wend
