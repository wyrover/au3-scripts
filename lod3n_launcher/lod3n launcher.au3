#NoTrayIcon
#include <A3LGDIPlus.au3> ; this is where the magic happens, people
#include <GuiCombo.au3>
#Include <File.au3>
#include <Array.au3>
Opt("MustDeclareVars", 0)

Global Const $AC_SRC_ALPHA      = 1
Global Const $ULW_ALPHA         = 2
Global $old_string = "", $runthis = ""
Global $launchDir = @DesktopDir

; Load PNG file as GDI bitmap
_GDIP_Startup()
$pngSrc = @scriptdir&"\LaunchySkin.png"
$hImage = _GDIP_ImageLoadFromFile($pngSrc)

; Extract image width and height from PNG
$width =  _GDIP_ImageGetWidth ($hImage)
$height = _GDIP_ImageGetHeight($hImage)

; Create layered window
$GUI = GUICreate("lod3n launcher", $width, $height, -1, -1, $WS_POPUP, $WS_EX_LAYERED)
SetBitMap($GUI, $hImage, 0)
; Register notification messages
GUIRegisterMsg($WM_NCHITTEST, "WM_NCHITTEST")
GUISetState()
WinSetOnTop($gui,"",1)
;fade in png background
for $i = 0 to 255 step 10
	SetBitMap($GUI, $hImage, $i)
next


; create child MDI gui window to hold controls
; this part could use some work - there is some flicker sometimes...
$controlGui = GUICreate("ControlGUI", $width, $height, 0,0,$WS_POPUP,BitOR($WS_EX_LAYERED,$WS_EX_MDICHILD),$gui)

; child window transparency is required to accomplish the full effect, so $WS_EX_LAYERED above, and
; I think the way this works is the transparent window color is based on the image you set here:
GUICtrlCreatePic(@ScriptDir & "\grey.gif",0,0,$width,$height)
GuiCtrlSetState(-1,$GUI_DISABLE)

; just a text label
GUICtrlCreateLabel("Type the name of a file on your desktop and press Enter",50,12,140,50)
GUICtrlSetBkColor(-1,$GUI_BKCOLOR_TRANSPARENT)
GUICtrlSetColor(-1,0xFFFFFF)

; combo box listing all items on desktop
$Combo = GuiCtrlCreateCombo("", 210, 12,250,-1)
GUICtrlSetFont ($Combo, 12)


; set default button for Enter key activation - renders outside GUI window
$goButton = GUICtrlCreateButton ( "Go",$width,$height,10,10,$BS_DEFPUSHBUTTON)

GUISetState()

; get list of files on desktop, show in combobox
$aFileList=_FileListToArray($launchDir)
_ArraySort($aFileList,0,1)
$FileList = _ArrayToString ( $aFileList, "|", 1)
GUICtrlSetData($Combo,$FileList)

AdlibEnable ( "GoAutoComplete", 1000 ) ; combo autocomplete every message loop = often incorrect
While 1
	$msg = GUIGetMsg()
	Select
		Case $msg = $GUI_EVENT_CLOSE
			ExitLoop
		Case $msg = $goButton
			$runthis = GUICtrlRead($Combo)
			ExitLoop
	EndSelect
WEnd
AdlibDisable ()



if $runthis <> "" then 
	if fileexists($launchDir & "\" & $runthis) then 
		beep(1000,50)
		beep(2000,50)
		_ShellExecute($runthis, "", $launchDir)
	EndIf
EndIf

GUIDelete($controlGui)
;fade out png background
for $i = 255 to 0 step -10
	SetBitMap($GUI, $hImage, $i)
next

; Release resources
_API_DeleteObject($hImage)
_GDIP_Shutdown()



func GoAutoComplete()
	_GUICtrlComboAutoComplete ($Combo, $old_string)
EndFunc

; ===============================================================================================================================
; Handle the WM_NCHITTEST for the layered window so it can be dragged by clicking anywhere on the image.
; ===============================================================================================================================
Func WM_NCHITTEST($hWnd, $iMsg, $iwParam, $ilParam)
  if ($hWnd = $GUI) and ($iMsg = $WM_NCHITTEST) then Return $HTCAPTION
EndFunc

; ===============================================================================================================================
; SetBitMap
; ===============================================================================================================================
Func SetBitmap($hGUI, $hImage, $iOpacity)
  Local $hScrDC, $hMemDC, $hBitmap, $hOld, $pSize, $tSize, $pSource, $tSource, $pBlend, $tBlend

  $hScrDC  = _API_GetDC(0)
  $hMemDC  = _API_CreateCompatibleDC($hScrDC)
  $hBitmap = _GDIP_BitmapCreateHBITMAPFromBitmap($hImage)
  $hOld    = _API_SelectObject($hMemDC, $hBitmap)
  $tSize   = DllStructCreate($tagSIZE)
  $pSize   = DllStructGetPtr($tSize  )
  DllStructSetData($tSize, "X", _GDIP_ImageGetWidth ($hImage))
  DllStructSetData($tSize, "Y", _GDIP_ImageGetHeight($hImage))
  $tSource = DllStructCreate($tagPOINT)
  $pSource = DllStructGetPtr($tSource)
  $tBlend  = DllStructCreate($tagBLENDFUNCTION)
  $pBlend  = DllStructGetPtr($tBlend)
  DllStructSetData($tBlend, "Alpha" , $iOpacity    )
  DllStructSetData($tBlend, "Format", $AC_SRC_ALPHA)
  _API_UpdateLayeredWindow($hGUI, $hScrDC, 0, $pSize, $hMemDC, $pSource, 0, $pBlend, $ULW_ALPHA)
  _API_ReleaseDC   (0, $hScrDC)
  _API_SelectObject($hMemDC, $hOld)
  _API_DeleteObject($hBitmap)
  _API_DeleteDC    ($hMemDC)
EndFunc


; I don't like AutoIt's built in ShellExec. I'd rather do the DLL call myself.
Func _ShellExecute($sCmd, $sArg="", $sFolder = "", $rState = @SW_SHOWNORMAL)
   $aRet = DllCall("shell32.dll", "long", "ShellExecute", _
      "hwnd",   0, _
      "string", "", _
      "string", $sCmd, _
      "string", $sArg, _
      "string", $sFolder, _
      "int",    $rState)
   If @error Then Return 0
	   
   $RetVal = $aRet[0]
   If $RetVal > 32 Then
	   Return 1
   else  
	Return 0
   EndIf
EndFunc