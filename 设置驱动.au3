#cs ----------------------------------------------------------------------------

 AutoIt 版本: 3.2.4.9(第一版)
 脚本作者: 
	Email: 
	QQ/TM: 
 脚本版本: 
 脚本功能: 

#ce ----------------------------------------------------------------------------

; 脚本开始 - 在这后面添加您的代码.
#include<GUIconstants.Au3>
Opt("TrayIconHide", 1)
Opt("GUICloseOnESC", 0) 

$DE = GUICreate("无效/有效驱动", 305, 145, -1, -1)
GUICtrlCreateLabel("计算机名 ", 10,10,70,15)

$PCName = GUICtrlCreateInput(@ComputerName, 85,10,150, 15, -1,0x00000020 )
GUICtrlSetLimit(-1,256)
$Status = GUICtrlCreateButton("查询",240,10,60,20,BitOr(0x0001,0x8000))
GUICtrlSetState($status, $GUI_FOCUS)
GUICtrlCreateLabel("USB 驱动 ", 10,35,90,15)
$UDisable = GUICtrlCreateButton("无效",105,35,60,20,BitOr(0x0001,0x8000))
$UEnable = GUICtrlCreateButton("有效",170,35,60,20,BitOr(0x0001,0x8000))
$USB = GUICtrlCreateLabel("", 240,37,60,15)

GUICtrlCreateLabel("光驱 ", 10,60,90,15)
$CDisable = GUICtrlCreateButton("无效",105,60,60,20,BitOr(0x0001,0x8000))

$CEnable = GUICtrlCreateButton("有效",170,60,60,20,BitOr(0x0001,0x8000))
$CD = GUICtrlCreateLabel("", 240,62,60,15)

GUICtrlCreateLabel("软驱", 10,85,90,15)
$FDisable = GUICtrlCreateButton("无效",105,85,60,20,BitOr(0x0001,0x8000))

$FEnable = GUICtrlCreateButton("有效",170,85,60,20,BitOr(0x0001,0x8000))
$FD = GUICtrlCreateLabel("", 240,87,60,15)

GUICtrlCreateLabel("高密度的软驱", 10,105,90,30)
$FHDisable = GUICtrlCreateButton("无效",105,110,60,20,BitOr(0x0001,0x8000))

$FHEnable = GUICtrlCreateButton("有效",170,110,60,20,BitOr(0x0001,0x8000))
$HFD = GUICtrlCreateLabel("", 240,112,60,15)
$Final = GUICtrlCreateLabel("", 102,132,190,15)
Opt("TrayIconHide", 1)
GUISetState (@SW_SHOW)
While 1
    $msg = GUIGetMsg()

    If $msg = $GUI_EVENT_CLOSE Then
        DllCall("user32.dll", "int", "AnimateWindow", "hwnd", $DE, "int", 1000, "long", 0x00090000);fade-out
        ExitLoop
    EndIf
    
    Select
    Case $msg = $Status
        _PathSet()
        
    Case $msg = $UDisable
        _PathSet()
        RegWrite($path & "\HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\USBSTOR", "Start", "REG_DWORD", "4")
        If Not @error Then 
            GUICtrlSetData( $USB, "无效")
            GUICtrlSetColor($USB,0xff0000)
            GUICtrlSetData( $Final,"(重启后生效 )")
        Else
            GUICtrlSetData( $USB, "错误！")
        EndIf
    Case $msg = $UEnable
        _PathSet()
        RegWrite($path & "\HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\USBSTOR", "Start", "REG_DWORD", "3")
        If Not @error Then 
            GUICtrlSetData( $USB, "有效")
            GUICtrlSetData( $Final,"(重启后生效 )")
        Else
            GUICtrlSetData( $USB, "错误！")
        EndIf
    Case $msg = $CDisable
        _PathSet()
        RegWrite($path & "\HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\Cdrom", "Start", "REG_DWORD", "4")
        If Not @error Then 
            GUICtrlSetData( $CD, "无效")
            GUICtrlSetColor($CD,0xff0000)
            GUICtrlSetData( $Final,"(重启后生效 )")
        Else
            GUICtrlSetData( $CD, "错误！")
        EndIf
    Case $msg = $CEnable
        _PathSet()
        RegWrite($path & "\HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\Cdrom", "Start", "REG_DWORD", "1")
        If Not @error Then 
            GUICtrlSetData( $CD, "有效")
            GUICtrlSetData( $Final,"(重启后生效 )")
        Else
            GUICtrlSetData( $CD, "错误！")
        EndIf
    Case $msg = $FDisable
        _PathSet()
        RegWrite($path & "\HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\Flpydisk", "Start", "REG_DWORD", "4")
        If Not @error Then 
            GUICtrlSetData( $FD, "无效")
            GUICtrlSetColor($FD,0xff0000)
            GUICtrlSetData( $Final,"(重启后生效 )")
        Else
            GUICtrlSetData( $FD, "错误！")
        EndIf
    Case $msg = $FEnable
        _PathSet()
        RegWrite($path & "\HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\Flpydisk", "Start", "REG_DWORD", "3")
        If Not @error Then 
            GUICtrlSetData( $FD, "有效")
            GUICtrlSetData( $Final,"(重启后生效 )")
        Else
            GUICtrlSetData( $FD, "错误！")
        EndIf      
    Case $msg = $FHDisable
        _PathSet()
        RegWrite($path & "\HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\Sfloppy", "Start", "REG_DWORD", "4")
        If Not @error Then 
            GUICtrlSetData( $hFD, "无效")
            GUICtrlSetColor($hFD,0xff0000)
            GUICtrlSetData( $Final,"(重启后生效 )")
        Else
            GUICtrlSetData( $hFD, "错误！")
        EndIf      
    Case $msg = $FHEnable
        _PathSet()
        RegWrite($path & "\HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\Sfloppy", "Start", "REG_DWORD", "3")
        If Not @error Then 
            GUICtrlSetData( $hFD, "有效")
            GUICtrlSetData( $Final,"(重启后生效 )")
        Else
            GUICtrlSetData( $hFD, "错误！")
        EndIf      

    EndSelect
    
WEnd

Func _ReadStatus()
    
    ; FOR USB<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    $USB_REG_Value = RegRead( $Path & "\HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\USBSTOR", "Start")
    If $USB_REG_Value = 3 Then
        GUICtrlSetData( $USB, "有效" )
    ElseIf $USB_REG_Value = 4 Then
        GUICtrlSetData( $USB, "无效")
        GUICtrlSetColor($USB,0xff0000)
    Else
        GUICtrlSetData( $USB, "Unknown")
    EndIf
    ;FOR CD<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    $CD_REG_Value = RegRead( $Path & "\HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\Cdrom", "Start")
    If $CD_REG_Value = 1 Then
        GUICtrlSetData( $CD, "有效" )
    ElseIf $CD_REG_Value = 4 Then
        GUICtrlSetData( $CD, "无效")
        GUICtrlSetColor($CD,0xff0000)
    Else
        GUICtrlSetData( $CD, "未知")
    EndIf
    ;FOR Floppy<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    $FD_REG_Value = RegRead( $Path & "\HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\Flpydisk", "Start")
    If $FD_REG_Value = 3 Then
        GUICtrlSetData( $FD, "有效" )
    ElseIf $FD_REG_Value = 4 Then
        GUICtrlSetData( $FD, "无效")
        GUICtrlSetColor($FD,0xff0000)
    Elseif $FD_REG_Value = 1 Then
        GUICtrlSetData( $FD, "System")
    ElseIf $FD_REG_Value = 2 Then
        GUICtrlSetData( $FD, "Auto Load")
    Else
        GUICtrlSetData( $FD, "未知")
    EndIf
    ;FOR High Capacity Floppy<<<<<<<<<<<<<<<<<
    $hFD_REG_Value = RegRead( $Path & "\HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\Sfloppy", "Start")
    If $hFD_REG_Value = 3 Then
        GUICtrlSetData( $hFD, "有效" )
    ElseIf $hFD_REG_Value = 4 Then
        GUICtrlSetData( $hFD, "无效")
        GUICtrlSetColor($hFD,0xff0000)
    Else
        GUICtrlSetData( $hFD, "未知")
    EndIf
    

EndFunc

Func _PathSet()
    $NameIP = GUICtrlRead($PCName)
    If $NameIP = "" Then
        MsgBox(16,"请输计算机名或IP", "请输入计算机名或IP")
    Else
    Global  $Path = "\\" & $NameIP
        ;MsgBox(0,"Path", $path)
        $ping = Ping($NameIP)
        If @error then 
            GUICtrlSetData($Final,"对不起，不能达到主机！")
            GUICtrlSetColor($final,0xff0000)
        Else
            GUICtrlSetData( $Final,"")
            _ReadStatus()
            
        EndIf
        
    EndIf
    
EndFunc
