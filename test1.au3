#cs ----------------------------------------------------------------------------

 AutoIt 版本: 3.2.4.9(第一版)
 脚本作者: 
	Email: 
	QQ/TM: 
 脚本版本: 
 脚本功能: 

#ce ----------------------------------------------------------------------------

; 脚本开始 - 在这后面添加您的代码.
#NoTrayIcon
;已知缺陷：程序未在启动时检测是否已注册过右键菜单
#include <GuiConstants.au3>
#include <file.au3>

Dim $szDrive, $szDir, $szFName, $szExt
Opt("GUIOnEventMode", 1)

GuiCreate("Unlocker辅助", 390, 120,-1, -1 , BitOR($WS_OVERLAPPEDWINDOW, $WS_CLIPSIBLINGS))
GUISetOnEvent($GUI_EVENT_CLOSE, "CloseClicked")

$CheckboxShell = GuiCtrlCreateCheckbox("注册右键菜单", 150, 40, 130, 20)
GUICtrlSetOnEvent($CheckboxShell, "OnCheckboxShell")

$Button_3 = GuiCtrlCreateButton("解锁文件...", 80, 80, 80, 30)
GUICtrlSetOnEvent($Button_3,"OnBrowserFile")

$Button_5 = GuiCtrlCreateButton("绿色退出", 260, 80, 80, 30)
GUICtrlSetOnEvent($Button_5,"ExitClicked")

$Label_6 = GuiCtrlCreateLabel("Unlocker 绿色辅助扩展。可使用右键菜单，或者直接浏览目标文件。", 10, 10, 370, 20)
GuiSetState()

While 1
Sleep(1000)
WEnd
;使用浏览对话框直接选择文件进行解锁处理
Func OnBrowserFile()
$var = FileOpenDialog("选择文件",@WorkingDir,"All (*.*)")
_PathSplit($var, $szDrive, $szDir, $szFName, $szExt)
RunWait('"' & @ScriptDir & '\unlocker.exe" ' & $var,$szDrive & $szDir)
EndFunc
;按X退出时如果右键菜单存在，会予以保留
Func CloseClicked()
Exit
EndFunc
;按绿色退出时，如果已注册右键菜单，则清除
Func ExitClicked()
If GUICtrlRead($CheckboxShell) == $GUI_CHECKED Then
  RegDelete("HKEY_CLASSES_ROOT\*\shellex\ContextMenuHandlers\UnlockerShellExtension")
  RegDelete("HKEY_CLASSES_ROOT\AllFilesystemObjects\shellex\ContextMenuHandlers\UnlockerShellExtension")
  RegDelete("HKEY_CLASSES_ROOT\CLSID\{DDE4BEEB-DDE6-48fd-8EB5-035C09923F83}")
  RegDelete("HKEY_CLASSES_ROOT\CLSID\UnlockerShellExtension")
  RegDelete("HKEY_CLASSES_ROOT\Directory\shellex\ContextMenuHandlers\UnlockerShellExtension")
  RegDelete("HKEY_CLASSES_ROOT\Folder\shellex\ContextMenuHandlers\UnlockerShellExtension")
  RegDelete("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths\Unlocker.exe")
  RegDelete("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Shell Extensions\Approved","{DDE4BEEB-DDE6-48fd-8EB5-035C09923F83}")
  RegDelete("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\Unlocker")
EndIf
Exit
EndFunc
;按勾选框的状态确定是否注册右unlocker右键菜单
Func OnCheckboxShell()
If GUICtrlRead($CheckboxShell) == $GUI_CHECKED Then
  RegWrite("HKEY_CLASSES_ROOT\*\shellex\ContextMenuHandlers\UnlockerShellExtension","","REG_SZ","{DDE4BEEB-DDE6-48fd-8EB5-035C09923F83}")
  RegWrite("HKEY_CLASSES_ROOT\AllFilesystemObjects\shellex\ContextMenuHandlers\UnlockerShellExtension","","REG_SZ","{DDE4BEEB-DDE6-48fd-8EB5-035C09923F83}")
  RegWrite("HKEY_CLASSES_ROOT\CLSID\{DDE4BEEB-DDE6-48fd-8EB5-035C09923F83}\InProcServer32","","REG_SZ",@ScriptDir & "\UnlockerCOM.dll")
  RegWrite("HKEY_CLASSES_ROOT\CLSID\{DDE4BEEB-DDE6-48fd-8EB5-035C09923F83}\InProcServer32","ThreadingModel","REG_SZ","Apartment")
  RegWrite("HKEY_CLASSES_ROOT\CLSID\{DDE4BEEB-DDE6-48fd-8EB5-035C09923F83}","","REG_SZ","UnlockerShellExtension")
  RegWrite("HKEY_CLASSES_ROOT\CLSID\UnlockerShellExtension","","REG_SZ","{DDE4BEEB-DDE6-48fd-8EB5-035C09923F83}")
  RegWrite("HKEY_CLASSES_ROOT\Directory\shellex\ContextMenuHandlers\UnlockerShellExtension","","REG_SZ","{DDE4BEEB-DDE6-48fd-8EB5-035C09923F83}")
  RegWrite("HKEY_CLASSES_ROOT\Folder\shellex\ContextMenuHandlers\UnlockerShellExtension","","REG_SZ","{DDE4BEEB-DDE6-48fd-8EB5-035C09923F83}")
  RegWrite("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths\Unlocker.exe","","REG_SZ",@ScriptDir & "\Unlocker.exe")
  RegWrite("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Shell Extensions\Approved","{DDE4BEEB-DDE6-48fd-8EB5-035C09923F83}","REG_SZ","UnlockerShellExtension")
  RegWrite("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\Unlocker","Language","REG_SZ","2052")
  
Else
  
  RegDelete("HKEY_CLASSES_ROOT\*\shellex\ContextMenuHandlers\UnlockerShellExtension")
  RegDelete("HKEY_CLASSES_ROOT\AllFilesystemObjects\shellex\ContextMenuHandlers\UnlockerShellExtension")
  RegDelete("HKEY_CLASSES_ROOT\CLSID\{DDE4BEEB-DDE6-48fd-8EB5-035C09923F83}")
  RegDelete("HKEY_CLASSES_ROOT\CLSID\UnlockerShellExtension")
  RegDelete("HKEY_CLASSES_ROOT\Directory\shellex\ContextMenuHandlers\UnlockerShellExtension")
  RegDelete("HKEY_CLASSES_ROOT\Folder\shellex\ContextMenuHandlers\UnlockerShellExtension")
  RegDelete("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths\Unlocker.exe")
  RegDelete("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Shell Extensions\Approved","{DDE4BEEB-DDE6-48fd-8EB5-035C09923F83}")
  RegDelete("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\Unlocker")
EndIf

EndFunc
