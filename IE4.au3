#cs ----------------------------------------------------------------------------

 AutoIt 版本: 3.2.4.9(第一版)
 脚本作者: 
	Email: 
	QQ/TM: 
 脚本版本: 
 脚本功能: 

#ce ----------------------------------------------------------------------------

; 脚本开始 - 在这后面添加您的代码.
#include<ie.au3>
#include<array.au3>
#include<inet.au3>


dim $ie
dim $wplF=@DesktopDir&'\BaiduTop100.wpl'
if FileExists($wplF) then
	TrayTip( '播放原有播放列表' ,'新排行榜下载中...',3)
	ShellExecute($wplF)
endif
dim $LnkList[1]=['']
dim $oLnk[1]=['']
dim $DownBody='下载列表(右击-目标另存为)<br><a href=http://ezdo.cn/au3/>查看更新</a><P>'
$a='http://list.mp3.baidu.com/list/newhits.html#top1'
$ie=_IECreate($a,0,1,1,1)
$lnks=_IELinkGetCollection($ie)
for $l in $lnks                ;listPage
	$nm=$l.innertext	
	$val=$l.href
	if StringRegExp($val,'word=\S+\+\S+')=1 then

	_ArrayAdd($lnkList,$val)
	_ArrayAdd($oLnk,$l.innertext)

	EndIf
next
for $l=1 to UBound($lnkList)-1 ;sogoPage
	$lnklist[$l]=Page2($lnklist[$l],'"http\S+/m\?ct=[^"]+word=(?:mp3|wma)[^"]+"')
	TrayTip( '分析中...' ,UBound($lnkList)-1-$l,Default)
Next

#region Write .WPL file
$wplFile='<?wpl version="1.0"?>'   & @crlf
$wplFile&='<smil>'   & @crlf
$wplFile&='<head>'   & @crlf
$wplFile&='<meta name="Generator" />'   & @crlf
$wplFile&='<author/>'   & @crlf
$wplFile&='<title>BaiDuTop100</title>'   & @crlf
$wplFile&='</head>'   & @crlf
$wplFile&='<body>'   & @crlf
$wplFile&='<seq>'   & @crlf

$l=1
for $l=1 to UBound($lnkList)-1 ;sogoPage
	$line=Page2($lnklist[$l],'"http[^"]+"')
	$wplFile&='  <media src="'&$line&'"/>'  & @crlf
	$DownBody &= '<a href='&$line& '>'&$oLnk[$l] & '</a><br>'
	TrayTip( '写入中播放列表...' ,UBound($lnkList)-1-$l,Default)
Next
$htmlnm=@DesktopDir&'\BaiduTop100.htm'
FileWrite($htmlnm,$DownBody)
_IENavigate($ie,$htmlnm)
$wplFile&='</seq>'   & @crlf
$wplFile&='</body>'   & @crlf
$wplFile&='</smil>'
$f=FileOpen($wplF,10)
FileWrite($f,$wplFile)
FileClose($f)
ShellExecute($wplF)
#endregion
Func Page2($s,$r)		;sogosource
	$s=_INetGetSource($s)
  	$s=StringRegExp($s,$r,1)
	Return StringReplace($s[0],'"','')
EndFunc

