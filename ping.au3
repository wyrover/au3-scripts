#cs ----------------------------------------------------------------------------

 AutoIt 版本: 3.2.4.9(第一版)
 脚本作者: 
	Email: 
	QQ/TM: 
 脚本版本: 
 脚本功能: 

#ce ----------------------------------------------------------------------------

; 脚本开始 - 在这后面添加您的代码.
;通过不停ping某个地址，来检测网络是否连通。

;如果连通，跳出对话框提示。并且播放声音提醒。

;具体可以自己改下，代码在下面：

#include <GUIConstants.au3>

GUICreate("Checking...",220,20);创建一个进度条窗口
$progressbar = GUICtrlCreateProgress (0,0,220,20)
;GUICtrlSetColor(-1,32250);
GUISetState ()

$i=1 ;进度条前进变量

While 1 ;use infinite loop since ExitLoop will get called
$var = Ping("192.168.0.1",400) ;192.168.0.1代表ping的地址，用来检测网络连接
If $var Then; Ping通则退出循环
ExitLoop
Else ;否则继续ping，并设置进度条
If $i=100 Then
$i=1
EndIf
GUICtrlSetData ($progressbar,$i)
$i=$i+1
EndIf
WEnd

GUIDelete();关掉进度条窗体
Msgbox(0,"Status","Connected",5)
while 1
SoundPlay("ring.wav",1) ;播放声音提醒，ring.wav位于当前目录
WEnd
