$FileName = "C:\Windows\Notepad.exe"
$LinkFileName = @DesktopDir & "\Text Editor.lnk"
$WorkingDirectory = @DesktopDir
$Icon = "C:\Windows\system32\SHELL32.dll"
$IconNumber = 57
$Description = "This is the text editor that comes with Windows"
$State = @SW_SHOWMAXIMIZED ;Can also be @SW_SHOWNORMAL or @SW_SHOWMINNOACTIVE

FileCreateShortcut($FileName,$LinkFileName,$WorkingDirectory,"",$Description,$Icon,"",$IconNumber,$State)