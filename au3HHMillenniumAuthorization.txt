; Millennium authorization dialog box for AutoIt:  #include <HHMillenniumAuthorization.au3>
; by Harvey E. Hahn -- latest update: 9 Sep 2006
;
; Copyright (C) 2006 by Arlington Heights Memorial Library.  Libraries 
; may freely use and adapt this script with due credit.  Commercial use 
; prohibited without written permission.
;
; IMPORTANT NOTE:
;   MouseClick(), PixelGetColor(), and any other AutoIt functions that use
;   X-Y coordinates are dependent on and affected by the screen resolution
;   (800x600, 1152x864, etc.) and by the Windows style (classic, XP, etc.);
;   the latter, for example, affects the Y-coordinate because the Windows
;   title bar can vary in height, depending on the style; all windows should
;   also be maximized for consistent coordinates

#include-once

;------------------------------------------------------------

Func GetAuthorization()  ; [15 Nov 2005; modified 8-9 Sep 2006]
	Dim $WinHandle
	Dim $txtbox1  ;control ID
	Dim $txtbox2  ;control ID
	Dim $OkButton  ;control ID
	Dim $CancelButton  ;control ID
	Dim $a
	Dim $Rtn
	;
	;set up the dialog box:
	$WinHandle = GUICreate ( "Please Enter Initials and Password" , 350, 200, 280, 250  )  ;width, height, left, top -- for 800x600
	;$WinHandle = GUICreate ( "Please Enter Initials and Password" , 350, 200, 510, 395  )  ;width, height, left, top -- for 1152x864
	GUICtrlCreateLabel( "&Initials:", 100, 70, 65, 15 )  ;left, top, width, height -- these coordinates are relative to the dialog box, not the whole window
	$txtbox1 = GUICtrlCreateInput ( "", 168, 65, 108, 19 )
	;GUICtrlCreateLabel( "<TAB>", 282, 70, 65, 15 )  ;behind username textbox
	GUICtrlCreateLabel( "&Password:", 100, 94, 65, 15 )
	$txtbox2 = GUICtrlCreateInput ( "", 168, 89, 108, 19, $ES_PASSWORD )
	;GUICtrlCreateLabel( "<ENTER>", 282, 94, 65, 15 )  ;behind password textbox
	$OkButton = GUICtrlCreateButton ("&OK", 111, 130, 49, 24 )
	$CancelButton = GUICtrlCreateButton ("&Cancel", 168, 130, 71, 24 )
	;
	;GUICtrlSetState( $OKButton, $GUI_DEFBUTTON )  ;OK button is the default button
	GUICtrlSetState( $txtbox1, $GUI_FOCUS )  ;give focus to the first textbox
	;
	GUISetState()  ;display the dialog box
	;
	$a = 0
	While 1
		$a = GUIGetMsg()
		Select
			Case $a = $GUI_EVENT_CLOSE  ; Esc key or [X] exit button
				$authi = ""
				$authp = ""
				$Rtn = 0
				ExitLoop
				
			Case $a = $CancelButton
				$authi = ""
				$authp = ""
				$Rtn = 0
				ExitLoop
				
			Case $a = $txtbox1
				GUICtrlSetState( $txtbox2, $GUI_FOCUS )  ; give focus to the password textbox
				
			Case $a = $txtbox2
				GUICtrlSetState( $OkButton, $GUI_FOCUS )  ; give focus to the OK button
				;GUICtrlSetBkColor( $OkButton, 0x669999 )  ; blue-green background for button
				
			Case $a = $OkButton
				$authi = GUICtrlRead($txtbox1)
				$authp = GUICtrlRead($txtbox2)
				$Rtn = 1
				ExitLoop
		EndSelect
	Wend
	;
	GUIDelete( $WinHandle )
	;
	Return $Rtn
EndFunc

;------------------------------------------------------------
