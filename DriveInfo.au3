$DriveArray = DriveGetDrive("all")
If Not @error Then
	$DriveInfo = ""
    For $DriveCount = 1 to $DriveArray[0]
        $DriveInfo &= StringUpper($DriveArray[$DriveCount])
		$DriveInfo &= " -  File System = " & DriveGetFileSystem($DriveArray[$DriveCount])
		$DriveInfo &= ",  Label = " & DriveGetLabel($DriveArray[$DriveCount])		
		$DriveInfo &= ",  Serial = " & DriveGetSerial($DriveArray[$DriveCount])		
		$DriveInfo &= ",  Type = " &  DriveGetType($DriveArray[$DriveCount])		
		$DriveInfo &= ",  Free Space = " & DriveSpaceFree($DriveArray[$DriveCount])		
		$DriveInfo &= ",  Total Space = " & DriveSpaceTotal($DriveArray[$DriveCount])		
		$DriveInfo &= ",  Status = " & DriveStatus($DriveArray[$DriveCount])	
		$DriveInfo &= @CRLF
    Next
	MsgBox(4096,"Drive Info", $DriveInfo)
EndIf