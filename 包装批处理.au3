#cs ----------------------------------------------------------------------------

 AutoIt 版本: 3.2.4.9(第一版)
 脚本作者: 
	Email: 
	QQ/TM: 
 脚本版本: 
 脚本功能: 

#ce ----------------------------------------------------------------------------

; 脚本开始 - 在这后面添加您的代码.
;批处理命令取自华军论坛 http://bbs.newhua.com/viewthread.php?tid=140551&extra=page%3D1
#cs bat
@ ECHO OFF
@ ECHO.
@ ECHO.
@ ECHO.                              说  明
@ ECHO -----------------------------------------------------------------------
@ ECHO 很多显卡在装了驱动之后，桌面右键会多出一项或多项菜单，这些功能并不实用，
@ ECHO 还会拖慢右键的弹出速度，以Intel的集成显卡为甚。迟纯的反应速度严重地影响
@ ECHO 了使用者的心情。我们最好清除它。《GhostXP电脑公司特别版》作者编。
@ ECHO -----------------------------------------------------------------------

regsvr32 /u /s igfxpph.dll
reg delete HKEY_CLASSES_ROOT\Directory\Background\shellex\ContextMenuHandlers /f
reg add HKEY_CLASSES_ROOT\Directory\Background\shellex\ContextMenuHandlers\new /ve /d {D969A300-E7FF-11d0-A93B-00A0C90F2719}
reg delete HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Run /v HotKeysCmds /f
reg delete HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Run /v IgfxTray /f

#ce bat

;写入批处理
$txt=StringRegExp(fileread(@scriptfullpath),'#cs bat'&'([\s\S]+)#'&'ce bat',2)
filewrite(@tempdir&'\~temp.bat',$txt[1])

;执行批处理
ShellExecuteWait(@tempdir&'\~temp.bat')

;删除批处理
filedelete(@tempdir&'\~temp.bat')
