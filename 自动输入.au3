#cs ----------------------------------------------------------------------------

 AutoIt 版本: 3.2.4.9(第一版)
 脚本作者: 
	Email: 
	QQ/TM: 
 脚本版本: 
 脚本功能: 

#ce ----------------------------------------------------------------------------

; 脚本开始 - 在这后面添加您的代码.
;一、QQ登录脚本
Run("D:\Program Files\QQ\QQ.exe")
WinWaitActive("QQ用户登录")
ControlFocus("QQ用户登录", "", "Edit1")
ControlSend("QQ用户登录", "", "Edit1", "ID")
ControlFocus("QQ用户登录", "", "Edit12")
ControlSend("QQ用户登录", "", "Edit12", "password")
ControlFocus("QQ用户登录", "", "Button15")
ControlClick("QQ用户登录", "", "Button15")

;使用时将路径、ID、password换成自己的QQ所在路径、用户名和密码。

;二、TM登录脚本
Run("C:\Program Files\Tencent\TM\TMShell.exe")
WinWaitActive("Tencent Messenger")
ControlFocus("Tencent Messenger", "", "Edit1")
ControlSetText("Tencent Messenger", "", "Edit1", "ID")
ControlFocus("Tencent Messenger", "", "Edit2")
ControlSetText("Tencent Messenger", "", "Edit2", "password")
ControlFocus("Tencent Messenger", "", "Button5")
ControlClick("Tencent Messenger", "", "Button5")