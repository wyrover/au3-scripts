; Convenient functions for AutoIt:  #include <HHMilCircFunctions.au3>
; by Harvey E. Hahn -- latest update: 8 Jun 2007  [added missing Dim's]
;
; Copyright (C) 2005-2006 by Arlington Heights Memorial Library.  Libraries 
; may freely use and adapt this script with due credit.  Commercial use 
; prohibited without written permission.
;
; IMPORTANT NOTE:
;   MouseClick(), PixelGetColor(), and any other AutoIt functions that use
;   X-Y coordinates are dependent on and affected by the screen resolution
;   (800x600, 1152x864, etc.) and by the Windows style (classic, XP, etc.);
;   the latter, for example, affects the Y-coordinate because the Windows
;   title bar can vary in height, depending on the style; all windows should
;   also be maximized for consistent coordinates

#include-once

;------------------------------------------------------------

Func ClickPatronTextBox()  ; [7 Nov 2005; modified for revised ClickAt, 31 Aug 2006]
	Dim $x
	Dim $y
	;
	$x = 220
	$y = 160
	ClickAt( $x, $y )
	Sleep(400)
EndFunc

;------------------------------------------------------------

Func EnterPatronBarcode( $barcode )  ; [7 Nov 2005; 25 Jan 2006; modified for revised ClickAt, 31 Aug 2006]
	Dim $wintitle
	;
	ClickPatronTextBox()
	Type( $barcode )  ;enter the barcode...
	Type( $Alt & "s" )  ;...followed by Alt-S (for "Search")
	Sleep(200)  ;give slight extra pause in case authorization dialog needs to appear
	; NOTE: there is an extremely remote chance that a patron barcode could be deleted,
	;       in which case an authorization dialog ("Enter Initials and Password") appears,
	;       which contains the text "create Patron records" and "edit Patron records"
	$wintitle = WinGetTitle("Enter Initials")  ;check if authorization dialog exists by attempting to get full title
	If $wintitle = "Enter Initials and Password" Then
		Type( $Alt & "c" )  ;press Alt-C to "Cancel" the dialog; unfortunately, III does not clear the barcode textbox, so...
		ClickAt( 280, 155 )  ;click into barcode textbox beyond barcode...  (800x600 mode)
		;ClickAt( 280, 200, 1 )  ;click into barcode textbox beyond barcode...  (1152x864 mode)
		Type( $Shift & "{HOME}" )  ;...hightlight the existing barcode...
		Type( "{DEL}" )  ;...and delete it
		Return False
	Else
		Return True
	EndIf
EndFunc

;------------------------------------------------------------

Func WaitForPatronDisplay()  ; [7 Nov 2005]
	Dim $Pxl1
	Dim $Pxl2
	Dim $Pxl3
	Dim $i2
	;
	; when patron record displays at end of search, only then is the barcode field cleared; thus,
	; check selected pixels in barcode field to see if field has been cleared after patron search:
	Do
		$Pxl1 = Hex( PixelGetColor( 122, 152 ), 6 )  ; three pixels from first digit "2" in barcode
		$Pxl2 = Hex( PixelGetColor( 124, 155 ), 6 )
		$Pxl3 = Hex( PixelGetColor( 124, 159 ), 6 )
	Until $Pxl1="FFFFFF" And $Pxl2="FFFFFF" And $Pxl3="FFFFFF"
	;
	; account for one or more possible "Message" windows appearing at this point!!!
	For $i2 = 1 To 10  ;workaround: arbitrarily send "OK" for up to 10 message windows (usually 1 or 2)
		Send($Alt & "o")
		Sleep(200)
	Next
	;
EndFunc

;------------------------------------------------------------

Func ClickItemTextBox()  ; [6 Jun 2007]
	ClickAt( 220, 160 )
EndFunc

;------------------------------------------------------------

Func EnterItemBarcode( $bcode )  ; [6 Jun 2007]
	ClickItemTextBox()
	Type( $bcode )  ; enter the barcode...
	Type( $Alt & "s" )  ; ...followed by Alt-S (for "Search")
	Sleep(200)  ; slight extra pause may be needed following a search
EndFunc

;------------------------------------------------------------

Func WaitForItemDisplay()  ; [6 Jun 2007]
	Dim $Pxl1
	Dim $Pxl2
	Dim $pxl3
	;
	; check selected pixels in barcode field to see if field has been cleared after item search:
	Do
		$Pxl1 = Hex( PixelGetColor( 130, 154 ), 6 )  ; 800x600 coords for maximized window
		$Pxl2 = Hex( PixelGetColor( 127, 156 ), 6 )
		$Pxl3 = Hex( PixelGetColor( 130, 157 ), 6 )
	Until $Pxl1="FFFFFF" And $Pxl2="FFFFFF" And $Pxl3="FFFFFF"
EndFunc

;------------------------------------------------------------

Func ClickFinesTab()  ; [7 Nov 2005; modified for revised ClickAt, 31 Aug 2006]
	Dim $x
	Dim $y
	;
	$x = 470
	$y = 270
	ClickAt( $x, $y )
	Sleep(400)
EndFunc

;------------------------------------------------------------

Func GetDisplayedFunctionType()  ; [15 Nov 2005; modified for revised ClickAt, 31 Aug 2006]
	Dim $BCol
	Dim $Pxl1
	Dim $Pxl2
	Dim $Pxl3
	Dim $Pxl4
	Dim $Pxl5
	Dim $Pxl6
	Dim $PxlB
	Dim $IsNotices
	;
	ClickAt( 200, 60 )  ;move cursor into toolbar area so that correct pixels will be read
	;
	$BCol = "CECFCE"  ;background color
	; check selected pixels in the toolbar area for the character "N" ("Notices" function):
	$Pxl1 = Hex( PixelGetColor( 7, 68 ), 6 )  ;same values for both 800x600 mode and 1152x864 mode
	$Pxl2 = Hex( PixelGetColor( 7, 61 ), 6 )
	$Pxl3 = Hex( PixelGetColor( 10, 64 ), 6 )
	$Pxl4 = Hex( PixelGetColor( 12, 68 ), 6 )
	$Pxl5 = Hex( PixelGetColor( 15, 71 ), 6 )
	$Pxl6 = Hex( PixelGetColor( 15, 64 ), 6 )
	If $Pxl1<>$BCol And $Pxl2<>$BCol And $Pxl3<>$BCol And $Pxl4<>$BCol And $Pxl5<>$BCol And $Pxl6<>$BCol Then
		$IsNotices = True  ; all selected pixels are nonbackground, so this is the "Notices" window
	Else
		$IsNotices = False  ; this is NOT the "Notices" window
	EndIf
	;
	If $IsNotices Then
		Return "N"
	Else
		Return ""
	EndIf
EndFunc

;------------------------------------------------------------

Func WaitForNoticesWindow()  ; [15 Nov 2005]
	Dim $FuncType
	;
	; check selected pixels in toolbar area for first letter of "Notices":
	Do
		$FuncType = GetDisplayedFunctionType()
	Until $FuncType = "N"
EndFunc

;------------------------------------------------------------

Func ChooseNoticesFunction()  ; [11 Nov 2005]
	Type( $Alt & "g" )  ;press Alt-G to "Go" to...
	Type( $Alt & "t" )  ;  ...Notices (Alt-T)
	Sleep(400)
EndFunc

;------------------------------------------------------------

Func CreateCharge( $Amount, $Reason )  ; [7 Nov 2005]
	Send( $Alt & "d" )  ; "Add Charge"
	Sleep(400)
	;
	CheckWindow()
	;
	Sleep(400)
	Send( $Alt & "a" )  ; "Amount"
	Sleep(400)
	Send( $Amount )
	Sleep(200)
	;
	Send( $Alt & "r" )  ; "Reason"
	Sleep(200)
	Send( $Reason )
	Sleep(200)
	;
	Send( $Alt & "o" )  ; "OK" button
	Sleep(400)
EndFunc

;------------------------------------------------------------
