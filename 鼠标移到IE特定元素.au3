#cs ----------------------------------------------------------------------------

 AutoIt 版本: 3.2.4.9(第一版)
 脚本作者: 
	Email: 
	QQ/TM: 
 脚本版本: 
 脚本功能: 

#ce ----------------------------------------------------------------------------

; 脚本开始 - 在这后面添加您的代码.
#include <IE.au3>
#Include <String.au3>

$oIE = _IECreate ()
$sHTML = ""
$sHTML &= "<h1>Top Of Screen</h1>" & @CR
$sHTML &= "<button id=Button0>Button0</button>" & @CR
$sHTML &= _StringRepeat ("<br>", 100 )
$sHTML &= "<h1>Middle of Screen</h1>" & @CR
$sHTML &= "<button id=Button1>Button1</button>" & @CR
$sHTML &= _StringRepeat ("<br>", 100 )
$sHTML &= "<h1>Bottom of Screen</h1>" & @CR
$sHTML &= "<button id=Button2>Button1</button>" & @CR
_IEDocWriteHTML ($oIE, $sHTML)


$oButton0 = _IEGetObjByName ($oIE, "Button0")
$oButton1 = _IEGetObjByName ($oIE, "Button1")
$oButton2 = _IEGetObjByName ($oIE, "Button2")



sleep(1000)
MouseToElement($oIE,$oButton0)

sleep(1000)
MouseToElement($oIE,$oButton1)

sleep(1000)
MouseToElement($oIE,$oButton2)

sleep(1000)
MouseToElement($oIE,$oButton0)


func MouseToElement($oIE,$oElem)
    local $x = _IEfindPosX($oElem)
    local $y = _IEfindPosY($oElem)
    $oElem.focus()
    
    $width = $oElem.offsetWidth
    $height = $oElem.offsetHeight
    $windowleft = $oIE.document.parentwindow.screenLeft
    $windowtop = $oIE.document.parentwindow.screenTop
    $mousex = $windowleft + $x + int($width/2) - $oIE.document.body.scrollLeft
    $mousey = $windowtop + $y + int($height/2) - $oIE.document.body.scrollTop
       
    mousemove($mousex,$mousey)
EndFunc

func _IEfindPosX($o_object)
    local $curleft = 0
    local $parent = $o_object
    if IsObj($parent) then
        while IsObj($parent)
            $curleft += $Parent.offsetLeft
            $parent = $Parent.offsetParent
        wend
    else
        local $objx = $o_object.x
        if IsObj($objx) then $curleft += $objx
    EndIf
    return $curleft
EndFunc

func _IEfindPosY($o_object)
    local $curtop = 0
    local $parent = $o_object
    if IsObj($parent) then
        while IsObj($parent)
            $curtop += $Parent.offsetTop
            $parent = $Parent.offsetParent
        wend
    else
        local $objy = $o_object.y
        if IsObj($objy) then $curtop += $objy
    EndIf
    return $curtop
EndFunc
