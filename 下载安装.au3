#cs ----------------------------------------------------------------------------

 AutoIt 版本: 3.2.4.9(第一版)
 脚本作者: 
	Email: 
	QQ/TM: 
 脚本版本: 
 脚本功能: 

#ce ----------------------------------------------------------------------------

; 脚本开始 - 在这后面添加您的代码.
#include<Inet.au3>
#include<ie.au3>
#include<systray.au3>
Opt("WinWaitDelay",1000)
Opt("WinDetectHiddenText",1)
Opt("WinTitleMatchMode",4)
;#cs
const $app='app.exe'
const $ViewUrl='http://www.flashget.com/cn/download.htm'    ;来源
$ie=_iecreate($ViewUrl)

const $DownUrl='http://flashget.newhua.com/down/antiarp4.3.1.exe'    ;下载地址

Dim $t="AntiARP Stand-alone Edition Setup"                ;安装窗口标题

$s=_INetGetSource($DownURL)
$s=StringRegExp($s,'http\S+.exe',3)
;$S[0]=$S[3]
;download
;msgbox(0,'',$s[0],4)
;exit
traytip('下载中','稍候...',3,1)
$SumSize=inetgetsize($s[0])
 
;exit 
InetGet($s[0],@tempdir&'\'& $app,1,1)
While @InetGetActive
  TrayTip("Downloading...", $s[0]& @cr& "Bytes = " & @InetGetBytesRead & '/' & $SumSize, 10, 1)
  Sleep(1000)
Wend

traytip('完成下载','安装...',3,1)

;run
Run(@tempdir&'\'& $app)
WinWait($t,"")
If Not WinActive($t,"") Then WinActivate($t,"")
WinWaitActive($t,"")
Send("{ALTDOWN}n{ALTUP}{ALTDOWN}a{ALTUP}{ALTDOWN}n{ALTUP}{ALTDOWN}n{ALTUP}{ALTDOWN}i{ALTUP}")
If WinWaitActive("ARP防火墙 - 提醒","",2) Then Send("{SPACE}")

WinWait($t,"")
If Not WinActive($t,"") Then WinActivate($t,"")
WinWaitActive($t,"")
Send("{ALTDOWN}f{ALTUP}")

_ieNavigate($ie,'http://www.antiarp.com/bbs/')

traytip('完成安装','设置中...',3,1)
;#ce
run(@programfilesdir&"\AntiARP Stand-alone Edition\AntiArp.exe")
$t='ARP防火墙单机个人版'
WinWaitActive($t,'',4)
$t=(_SysTrayIconIndex($t,2)) 
$t=_SysTrayIconPos($t)
MouseClick('menu',$t[0]+2,$t[1]+2,1,0)
Send('{down 3}{enter}')
WinWaitActive("设置",'',2)
ControlCommand("设置",'','TRzPageControl1','tableft','')
ConsoleWrite(WinWaitActive("设置",'允许修改浏览器首页',2)&'<<<<<>>>>>')
ControlClick('设置','允许修改浏览器首页','TRzCheckBox2')
WinWaitActive('ARP防火墙 - 询问','')
ControlClick('ARP防火墙 - 询问','','Button1')
WinWaitActive('设置','')
ControlClick('last','','TRzBitBtn3')

TrayTip('重新启动','使设置生效',Default)

WinActivate('ARP防火墙单机个人版 v4.3.1 【试用版】')
Send('!F{up}{enter}') ;WinMenuSelectItem('last','','文件(&F)','退出')
WinWaitActive('ARP防火墙 - 询问','',3)
ControlClick('last','','Button1')
run(@programfilesdir&"\AntiARP Stand-alone Edition\AntiArp.exe")
Exit

