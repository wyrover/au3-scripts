#cs ----------------------------------------------------------------------------

 AutoIt 版本: 3.2.4.9(第一版)
 脚本作者: 
	Email: 
	QQ/TM: 
 脚本版本: 
 脚本功能: 

#ce ----------------------------------------------------------------------------

; 脚本开始 - 在这后面添加您的代码.
#cs ----------------------------------------------------------------------------
AutoIt 版本: 3.2.5.7(第一版)
脚本作者: a1727
Email: a1727@126.com
QQ/TM: 
脚本版本: 0.0.0.1
脚本功能: 
#ce ----------------------------------------------------------------------------
#include <GUIConstants.au3>
$old_reg = RegRead("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion", "DevicePath")
$s_Path1 = "C:\"  ;预定义根目录
#Region ### START Koda GUI section ### Form=d:\me\support\桌面\b b\aform1.kxf
$AForm1_1 = GUICreate("设置设备驱动包搜索路径  作者:a1727  Email:a1727@126.com", 546, 394, 314, 169, -1, 0)
$Group1 = GUICtrlCreateGroup("添加驱动路径", 232, 8, 297, 305)
$Input1 = GUICtrlCreateInput("未选择目录", 240, 32, 201, 21,$ES_READONLY)
GUICtrlSetBkColor (-1, 0xFFFF80)
$Edit1 = GUICtrlCreateEdit($old_reg, 24, 32, 185, 273,BitOR($ES_AUTOVSCROLL,$ES_WANTRETURN,$ES_READONLY))
GUICtrlSetBkColor (-1, 0xFFFF80)
$Edit2 = GUICtrlCreateEdit("", 240, 72, 201, 233,BitOR($ES_AUTOVSCROLL,$ES_WANTRETURN,$ES_READONLY))
GUICtrlSetBkColor (-1, 0xFFFF80)
$xzml = GUICtrlCreateButton("选择目录", 448, 32, 75, 25, 0)
$yl = GUICtrlCreateButton("预浏览", 448, 72, 75, 25, 0)
GUICtrlSetState($yl, $GUI_DISABLE)
GUICtrlCreateGroup("", -99, -99, 1, 1)
$Group2 = GUICtrlCreateGroup("目前驱动路径", 16, 8, 201, 305)
GUICtrlCreateGroup("", -99, -99, 1, 1)
$mssz = GUICtrlCreateButton("马上设置", 16, 328, 155, 49, 0)
GUICtrlSetState($mssz, $GUI_DISABLE)
$hfyz = GUICtrlCreateButton("恢复原值", 192, 328, 155, 49, 0)
GUICtrlSetState($hfyz, $GUI_DISABLE)
$hyxpmrz = GUICtrlCreateButton("还原XP默认值", 368, 328, 155, 49, 0)
GUISetState(@SW_SHOW)
#EndRegion ### END Koda GUI section ###
While 1
$nmsg = GuiGetMsg() ;捕获窗体事件
Select
  Case $nmsg=$GUI_EVENT_CLOSE ;如果用户点击关闭按钮就退出程序
   Exit
  Case $nmsg = $xzml 
   $Path1 = FileSelectFolder("选择驱动集目录", "", "",$s_Path1)
   $Path2 = $Path1
   If Not @error Then
    If StringRight($Path1, 1) = "\" Then  ;判断是否为根目录
     GuiCtrlSetData($Input1, $Path1 & "*.*")  ;是
    Else
     GuiCtrlSetData($Input1, $Path1 & "\*.*") ;否
    endif
    GUICtrlSetState($yl, $GUI_ENABLE)
   endif
  Case $nmsg = $yl 
   DevicePath($Path1,$Path2)  ;调用DevicePath函数,获取驱动目录列表。
   If StringRight($Path1, 1) = "\" Then $Path2 = StringReplace ($Path2,"[url=file://\\]\\","\[/url]")  ;如果选择的目录是根目录，就将"\\"改为"\"。
   GUICtrlSetData($Edit2, $Path2,"")
   GUICtrlSetState($mssz, $GUI_ENABLE)
  Case $nmsg = $mssz
   $new_reg = RegRead("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion", "DevicePath")
   RegWrite ( "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion","DevicePath", "REG_EXPAND_SZ", $new_reg & ";" & $Path2)
   GUICtrlSetState($hfyz, $GUI_ENABLE)
   GuiCtrlSetData($Input1, "未选择目录")
   GUICtrlSetState($yl, $GUI_DISABLE)
   GUICtrlSetData($Edit2, "")
   GUICtrlSetState($mssz, $GUI_DISABLE)
   GUICtrlSetData($Edit1,RegRead("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion", "DevicePath"))
  Case $nmsg = $hfyz
   RegWrite ( "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion","DevicePath", "REG_EXPAND_SZ", $old_reg)
   GuiCtrlSetData($Input1, "未选择目录")
   GuiCtrlSetData($Edit1, $old_reg)
   GUICtrlSetState($mssz, $GUI_DISABLE)
   GUICtrlSetState($yl, $GUI_DISABLE)
   GUICtrlSetData($Edit2, "")
   GUICtrlSetState($hfyz, $GUI_DISABLE)
  Case $nmsg = $hyxpmrz
   RegWrite ( "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion","DevicePath", "REG_EXPAND_SZ", "%SystemRoot%\inf")
   GuiCtrlSetData($Input1, "未选择目录")
   GUICtrlSetData($Edit1,RegRead("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion", "DevicePath"))
   GUICtrlSetData($Edit2,"")
   GUICtrlSetState($yl, $GUI_DISABLE)
   GUICtrlSetState($mssz, $GUI_DISABLE)
  Case Else
  ContinueLoop
EndSelect
WEnd
Func DevicePath($Dir1,ByRef $Dir2)  ;定义$dir1为值传递参数，$dir2为引用参数
If StringRight($Dir1, 4) <> "\*.*" Then
  $search = FileFindFirstFile($Dir1 & "\*.*")
Else
  $search = FileFindFirstFile($Dir1)
EndIf
While 1
  $file = FileFindNextFile($search )
  If @error Then ExitLoop
  If FileGetAttrib ($Dir1 & "\" & $file)="D" Then  ;判断找到的是否目录。
   $Dir2 = $Dir2 & ";" & $Dir1 & "\" & $file  ;将找到的目录名增加到变量中。
   DevicePath($Dir1 & "\" & $file,$Dir2);递归搜索
  EndIf
WEnd
FileClose($search);; 关闭搜索句柄
EndFunc
